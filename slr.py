import numpy as np
import math

class SingleLinearRegression:
    def average(self,data):
        sum = 0
        for i in data:
            sum += i
        return sum / len(data)

    def totalSumSquares(self,data):
        sum = 0
        for i in data:
            sum += math.pow(i - self.average(data),2)
        return sum

    def Variance(self,data):
        return self.totalSumSquares(data)/(len(data)-1)

    def StandardDeviation(self,data):
        return math.sqrt(self.Variance(data))

    def SumCrossDeviation(self,x,y):
        avgx = self.average(x)
        avgy = self.average(y)
        return np.sum(x*y - x*avgy - avgx*y + avgx*avgy)

    def PCC(self,x,y):
        return self.SumCrossDeviation(x,y)/math.sqrt(self.totalSumSquares(x)*self.totalSumSquares(y))

    def r2(self,x,y):
        return math.pow(self.PCC(x,y),2)

    def beta(self,x,y):
        return (self.PCC(x,y))*(self.StandardDeviation(y)/self.StandardDeviation(x))

    def alpha(self,x,y):
        return self.average(y)-self.beta(x,y)*self.average(x)




if __name__ == '__main__':
    x = np.array([2, 4, 6, 8, 10, 12, 14])
    y = np.array([30,25,22,18,15,11,10])
    obj = SingleLinearRegression()
    print("Pearson correlation coefficient : %f"%obj.PCC(x,y))
    print("R2 : %f"%obj.r2(x,y))
    print("Beta : %f"%obj.beta(x,y))
    print("Alpha : %f"%obj.alpha(x,y))