###Regressão Linear Simples[1]###

A análise de regressão estuda a relação entre uma variável chamada a variável dependente e outras variáveis chamadas variáveis independentes.
A relação entre elas é representada por um modelo matemático, que associa a variável dependente com as variáveis independentes.
Este modelo é designado por modelo de regressão linear simples (MRLS) se define uma relação linear entre a variável dependente e uma variável independente.

###Equação da Regressão Linear[5]###

Para se estimar o valor esperado, usa-se de uma equação, que determina a relação entre ambas as variáveis.

Y =  a + ßX+ e

Em que:

Y - variável explicada ou dependente (aleatória);

X - variável explicativa ou independente medida sem erro (não aleatória);

a - coeficiente de regressão, que representa o intercepto;

ß - coeficiente de regressão, que representa o declive (inclinação);

e - erro aleatório ou estocástico, onde se procuram incluir todas as influências no comportamento da variável Y que não podem ser explicadas linearmente pelo comportamento da variável X.

###Diagrama de dispersão###

Os diagramas de dispersão ou gráficos de dispersão são representações de dados de duas ou mais variáveis que são organizadas em um gráfico. O gráfico de dispersão utiliza coordenadas cartesianas para exibir valores de um conjunto de dados. Os dados são exibidos como uma coleção de pontos, cada um com o valor de uma variável determinando a posição no eixo horizontal e o valor da outra variável determinando a posição no eixo vertical (em caso de duas variáveis).[3]

Este diagrama permite decidir empiricamente[4]:

* se um relacionamento linear entre as variáveis X e Y deve ser assumido
* se o grau de relacionamento linear entre as variáveis é forte ou fraco, conforme o modo como se situam os pontos em redor de uma recta imaginária que passa através do enxame de pontos.


###Coeficiente de correlação de Pearson[6]###

Em estatística descritiva, o coeficiente de correlação de Pearson, mede o grau da correlação (e a direcção dessa correlação - se positiva ou negativa) entre duas variáveis de escala métrica (intervalar ou de rácio/razão).

Este coeficiente, normalmente representado por ? ou r assume apenas valores entre -1 e 1.

###Tipos de correlação[8]###
* Correlação negativa r < Zero, quanto mais próximo de -1 maior é a correlação negativa.
* Sem correlação linear r = Zero, quanto mais próximo de 0, menor é a correlação linear.
* Correlação positiva r > Zero, quanto mais próximo de 1, maior é a correlação positiva.

###A correlação linear em relação ao valor de r[8]###
* Entre -1 e 0: A correlação pode ser negativa forte (quando tende mais para -1) ou negativa fraca (quando tende para 0);
* Entre 0 e +1: A correlação pode ser positiva forte (quando tende para +1) ou positiva fraca (quando tende para 0);
* No pono 0 a correlação não exite.

###Interpretando Coeficiente de correlação de Pearson[7]###
* 0.9 para mais ou para menos indica uma correlação muito forte.
* 0.7 a 0.9 positivo ou negativo indica uma correlação forte.
* 0.5 a 0.7 positivo ou negativo indica uma correlação moderada.
* 0.3 a 0.5 positivo ou negativo indica uma correlação fraca.
* 0 a 0.3 positivo ou negativo indica uma correlação desprezível.

### Referências ###

* [1,2,4]Regressão linear simples, fmachado, https://www.ime.usp.br/~fmachado/MAE229/AULA10.pdf
* [3]Diagrama de dispersão, wikipedia, https://pt.wikipedia.org/wiki/Gr%C3%A1fico_de_dispers%C3%A3o
* [5]Regressão linear, wikipedia, https://pt.wikipedia.org/wiki/Regress%C3%A3o_linear
* [6]Coeficiente de correlação de Pearson, wikipedia, https://pt.wikipedia.org/wiki/Coeficiente_de_correla%C3%A7%C3%A3o_de_Pearson
* [7]Hinkle DE, Wiersma W, Jurs SG. Applied Statistics for the Behavioral Sciences. 5th ed. Boston: Houghton Mifflin; 2003.
* [8]Correlação e Regressão Linear - Tratamento e Análise de Dados e Informações - EACH/USP, youtube, https://www.youtube.com/watch?v=dWZ7oBiarsA

### Autor ###

* de Freitas, P.C.P